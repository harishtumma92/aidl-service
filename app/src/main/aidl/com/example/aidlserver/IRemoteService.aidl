// IRemoteService.aidl
package com.example.aidlserver;
import com.example.aidlserver.IRemoteServiceCallback;


// Declare any non-default types here with import statements

interface IRemoteService {
    /**
     * Demonstrates some basic types that you can use as parameters
     * and return values in AIDL.
     */
       boolean registerCallback(IRemoteServiceCallback callback);
    	boolean unregisterCallback(IRemoteServiceCallback callback);
    	String getMessage();
}
