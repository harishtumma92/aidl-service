package com.example.aidlserver

import android.R
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.hardware.SensorManager
import android.os.*
import timber.log.Timber


class MyService : Service(), MySesonrManager.SensorLisner {

    val callbacks = RemoteCallbackList<IRemoteServiceCallback>()
    lateinit var mNM: NotificationManager
    var mValue: Long = 0
    private val REPORT_MSG = 1
    var sensor: FloatArray? = null
    lateinit var senSesonrManager: MySesonrManager

    private val mBinder: IRemoteService.Stub = object : IRemoteService.Stub() {
        @Throws(RemoteException::class)
        override fun unregisterCallback(callback: IRemoteServiceCallback): Boolean {
            var flag = false
            if (callback != null) {
                Timber.i("unRegister call")
                flag = callbacks.unregister(callback)
                Timber.d("unRegister callback:$flag")
            }
            return flag
        }

        @Throws(RemoteException::class)
        override fun registerCallback(callback: IRemoteServiceCallback): Boolean {
            var flag = false
            if (callback != null) {
                Timber.i("Register call")
                flag = callbacks.register(callback)
                Timber.d("Register callback:$flag")
            }
            return flag
        }

        @Throws(RemoteException::class)
        override fun getMessage(): String {
            Timber.i("get message called")
            return "Hello World!"
        }
    }


    override fun onCreate() {
        super.onCreate()
        Timber.i("onCreate called")
        senSesonrManager = (application as MyApplication).mySesonrManager
        senSesonrManager.setSensorListner(this)
        mNM = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        showNotification()
        mHandler.sendEmptyMessage(REPORT_MSG);

    }

    override fun onBind(intent: Intent): IBinder {
        Timber.i("Bound service")
        return mBinder
    }

    private fun showNotification() {

        val notification: Notification = Notification.Builder(this)
            .setSmallIcon(R.mipmap.sym_def_app_icon) // the status icon
            //.setTicker(text) // the status text
            .setWhen(System.currentTimeMillis()) // the time stamp
            .setContentTitle("remote_service_label") // the label of the entry
            .setContentText("text") // the contents of the entry
            //.setContentIntent(contentIntent) // The intent to send when the entry is clicked
            .build()
        // Send the notification.
        // We use a string id because it is a unique number.  We use it later to cancel.
        mNM.notify(11, notification)
    }


    /* private  val  mHandler :Handler= object : Handler(){

     }*/

    private val mHandler: Handler = @SuppressLint("HandlerLeak")
    object : Handler() {
        override fun handleMessage(msg: Message) {
            when (msg.what) {
                REPORT_MSG -> {
                    Timber.i("Report messge")
                    // Up it goes.
                    val value: Long = ++mValue

                    // Broadcast to all clients the new value.
                    val N: Int = callbacks.beginBroadcast()
                    var i = 0
                    while (i < N) {
                        try {
                            Timber.d("broadcast count:$i")
                            Timber.d("value:$value")
                            if (sensor == null) {
                                var f = FloatArray(3)
                                f[0] = 0.0f
                                f[1] = 0.0f
                                f[2] = 0.0f
                                sensor = f
                            }

                            callbacks.getBroadcastItem(i).sensorOnChanged(sensor)
                        } catch (e: RemoteException) {
                            Timber.e(e.toString())
                            // The RemoteCallbackList will take care of removing
                            // the dead object for us.
                        }
                        i++
                    }
                    callbacks.finishBroadcast()

                    // Repeat every 8 milli second.
                    sendMessageDelayed(obtainMessage(REPORT_MSG), 1 * 8)
                }
                else -> super.handleMessage(msg)
            }
        }
    }

    override fun onDestroy() {
        Timber.i("Service is onDestory..")
        mHandler.removeMessages(REPORT_MSG)
        super.onDestroy()
    }

    override fun onSensorChanged(array: FloatArray) {
        Timber.d("sensor aaa:${array.toString()}")
        sensor = array
    }

}
