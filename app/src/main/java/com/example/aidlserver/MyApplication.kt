package com.example.aidlserver

import android.app.Application
import timber.log.Timber

open class MyApplication:Application() {
    lateinit var mySesonrManager:MySesonrManager
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        mySesonrManager= MySesonrManager(this)
    }

    override fun onTerminate() {
        super.onTerminate()
            mySesonrManager.unRegister()
    }
}

