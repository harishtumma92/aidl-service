package com.example.aidlserver

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import timber.log.Timber

class MySesonrManager(context: Context ) : SensorEventListener {
    private lateinit var sensorManager: SensorManager
    val arr2 = FloatArray(3)
    var sensorListener:SensorLisner?=null
    private var rotation: Sensor? = null
    init {
         Timber.d("MySesonrManager()")
        sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        initSensor()
    }


    fun setSensorListner(sensorListener:SensorLisner){
        this.sensorListener=sensorListener
        Timber.d("setSensorListner")
    }

    fun initSensor(){
        rotation = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
        if (rotation != null) {
           Timber.d("TYPE_ROTATION_VECTOR sensor exist")
            sensorManager.registerListener(this,rotation, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Timber.d("TYPE_ROTATION_VECTOR not exist")
            // Failure! No magnetometer.
        }
    }

    override fun onSensorChanged(p0: SensorEvent?) {
       // Log.d("hhh",p0?.values!!?.get(0).toString())
        arr2[0]= p0?.values!!?.get(0)
        arr2[1]= p0?.values!!?.get(1)
        arr2[2]= p0?.values!!?.get(2)
        Timber.d("sensor value: ${arr2[2].toString()}" )
        if(sensorListener!=null) {
            sensorListener?.onSensorChanged(arr2)
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {

    }

    fun unRegister(){
        var v=sensorManager.unregisterListener(this)
        Timber.d("unregister sensor lisner: $v")
    }


    interface SensorLisner{
        fun onSensorChanged(array:FloatArray)
    }

}